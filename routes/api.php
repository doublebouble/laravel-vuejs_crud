<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test', function (){
    return response()->json(['test-data'=>'all is ok!']);
});

Route::post('login', 'AuthController@login');

Route::post('recover', 'AuthController@recover');

Route::post('password/reset/{token}', 'Auth\ResetPasswordController@postReset')->name('password.resetPost');

Route::get('/get-languages', 'Configurations\ConfigurationsTranslateController@getLanguages')->name('translate.getLanguages');

Route::group(['middleware'=>['jwt.auth', 'jwt.refresh']],function (){

    Route::post('logout', 'AuthController@logout');

    Route::get('/auth-test', function (){
        return response()->json(['test-data'=>'all is ok!']);
    });

    Route::get('/logged-user', 'UserController@showLoggedUserData')->name('user.showLoggedUserData');


    Route::get('/customers','CustomersController@index');

    Route::get('/customers/{customer}','CustomersController@show');

    Route::post('/customers','CustomersController@store');

    Route::post('/customers/{customer}/update','CustomersController@update');

    Route::delete('/customers/{customer}/delete','CustomersController@destroy');


    Route::get('/open-hours', 'OpenHoursController@index');

    Route::get('/open-hours/{openHour}', 'OpenHoursController@show');

    Route::post('/open-hours', 'OpenHoursController@store');

    Route::post('/open-hours/{openHour}/update', 'OpenHoursController@update');

    Route::post('/open-hours/{openHour}/delete', 'OpenHoursController@destroy');


    Route::get('/rooms', 'RoomsController@index');

    Route::get('/rooms/{room}', 'RoomsController@show');

    Route::post('/rooms', 'RoomsController@store');

    Route::post('/rooms/{room}/update', 'RoomsController@update');

    Route::delete('/rooms/{room}/delete', 'RoomsController@destroy');


    Route::get('/reservations', 'ReservationsController@index');

    Route::get('/reservations/{reservation}', 'ReservationsController@show');

    Route::post('/reservations', 'ReservationsController@store');

    Route::post('/create-reservation-by-admin', 'ReservationsController@storeByAdmin');

    Route::post('/update-reservation-by-admin/{reservation}', 'ReservationsController@updateByAdmin');

    Route::delete('/reservations/{reservation}/delete','ReservationsController@destroy');
});

Route::group(['prefix'=>'configurations', 'namespace' => 'Configurations','middleware'=>['jwt.auth', 'jwt.refresh', 'admin']],function () {

    Route::get('/get-words', 'ConfigurationsTranslateController@getWords')->name('translate.getWords');

    Route::post('/update-words', 'ConfigurationsTranslateController@updateWords')->name('translate.updateWords');

    Route::post('/add-language', 'ConfigurationsTranslateController@addLanguage')->name('translate.addLanguage');

    Route::delete('/delete-language/{code}', 'ConfigurationsTranslateController@deleteLanguage')->name('translate.deleteLanguage');
});
