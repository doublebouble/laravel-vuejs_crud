<?php

namespace App\Http\Controllers\Configurations;

use App\Models\Language;
use App\Models\LanguageWord;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ConfigurationsTranslateController extends Controller
{
   function getWords ()
   {
       $data = request()->all();

       $words = LanguageWord::select(['id','alias',$data['lang']])->get();

       $listLanguages = Language::all();

       $d = [
           'words' => $words,
           'listLanguages' => $listLanguages,
       ];

       return response()->json(['success' => true, 'data'=>$d]);
   }


   function updateWords ()
   {
       $data = request()->all();


       $fileContent = "<?php \n return [\n";
       foreach ($data as $element) {
           if (isset($element['id'])) {
               LanguageWord::find($element['id'])->update($element);
               $fileContent .= "'".$element['alias']."' => '".$element[$data['lang']]."', \n";
           }
       }
       $fileContent .="];";
       if (!is_dir(resource_path("lang/".$data['lang']))) {
           mkdir(resource_path("lang/".$data['lang']), 0777, true);
       }

       File::put(resource_path('lang/'.$data['lang'].'/total.php'),$fileContent);

       Artisan::call('vue-i18n:generate');

       exec('npm run dev 2>&1');

       $words = LanguageWord::select(['id','alias',$data['lang']])->get();

       return response()->json(['success' => true, 'data'=>$words]);
   }


   function getLanguages ()
   {
       $listLanguages = Language::all();

       return response()->json(['success' => true, 'data'=>$listLanguages]);
   }


   function addLanguage ()
   {
       $data = request()->all();
       $isIssueLang = Language::whereCode($data['code'])->first();

       if (!is_null($isIssueLang)) {

           return response()->json(['success' => false, 'message'=>'This language already exists'], 500);
       }

       Language::create($data);
       DB::statement("ALTER TABLE `language_words` ADD `".$data['code']."` VARCHAR(255) NOT NULL AFTER `deleted_at`;");

       $listLanguages = Language::all();

       $addedLang = $data['code'];

       $words = LanguageWord::select(['id', 'alias', $addedLang])->get();

       $d = [
           'listLanguages' => $listLanguages,
           'addedLang' => $addedLang,
           'words' => $words,
       ];

       return response()->json(['success' => true, 'data'=>$d]);
   }


   function deleteLanguage ($code)
   {
       Language::whereCode($code)->delete();
       DB::statement("ALTER TABLE `language_words` DROP `".$code."`");

       $listLanguages = Language::all();

       $firstLang = Language::orderBy('id', 'ASC')->first();

       $lang = $firstLang['code'];

       $words = LanguageWord::select(['id', 'alias', $lang])->get();

       $d = [
           'listLanguages' => $listLanguages,
           'lang' => $lang,
           'words' => $words,
       ];

       return response()->json(['success' => true, 'data'=>$d]);
   }
}
