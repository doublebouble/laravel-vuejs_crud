<?php

namespace App\Http\Controllers\Admin;

use App\Models\LeaveRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Child;
use App\Models\Role;
use App\Models\UserLeave;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;
use Tymon\JWTAuth\Facades\JWTAuth;

class UsersController extends Controller
{

    function getAdminUsers ()
    {
        $requestData = request()->all();
        $years = LeaveRequest::getYearsListLeaveRequest();

        $supervisors = User::getAllSupervisors();
        $supervisorsModal = User::getAllSupervisorsModal();

        $usersList = User::all();

        $users = User::getUsers($requestData);
        $users = $this->getRoleNameForUser($users);

        return response()->json([
            'years' => $years,
            'users' => $users,
            'supervisors' => $supervisors,
            'supervisorsModal' => $supervisorsModal,
            'usersList' => $usersList,
        ]);
    }


    function getAdminUsersWithFilter ()
    {
        $requestData = request()->all();
        $users = User::getUsers($requestData);
        $users = $this->getRoleNameForUser($users);
        $supervisorsModal = User::getAllSupervisorsModal();

        return response()->json([
            'users' => $users,
            'supervisorsModal' => $supervisorsModal,
        ]);
    }


    function deleteUser()
    {
        $requestData = request()->all();
        User::find(request()->deleteUserId)->delete();

        $users = User::getUsers($requestData);
        $users = $this->getRoleNameForUser($users);
        $supervisorsModal = User::getAllSupervisorsModal();

        return response()->json([
            'users' => $users,
            'supervisorsModal' => $supervisorsModal,
        ]);
    }


    function updateUserProfile()
    {
        $requestData = request()->all();
        $userId = $requestData['userProfile']['id'];

        if (!User::checkIban($requestData['userProfile']['iban'])) {
            return response()->json(['success' => false, 'message'=>'Invalid IBAN'], 500);
        }

        $user = User::with('children')->find($userId);
        try {
            $userProfile = $requestData['userProfile'];

            if (isset($userProfile['transport'])) {
                $userProfile['transport'] = implode(',', $userProfile['transport']);
            }

            $user->update($userProfile);
            $users = User::getUsers($requestData);
            $users = $this->getRoleNameForUser($users);
            $supervisors = User::getAllSupervisors();
            $ds = [
                'users' => $users,
                'supervisors' => $supervisors
            ];
            return response()->json($ds);
        }
        catch (Exception $e) {
            return response()->json(['success' => false, 'message'=>$e->getMessage()], 501);
        }
    }


    function UpdateUserProfilePicture($id, Request $request)
    {
        $user = User::with('children')->find($id );

        $this->validate($request,[
            'file'=> 'mimes:png,jpeg,jpg|max:1024'
        ]);

        if(!$request->hasFile('profile-picture'))
        {
            return response()->json(['success' => false, 'message' => 'File not provided'], 404);
        }

        try {
            $user->updateProfilePicture($request->file('profile-picture'));

        }
        catch (Exception $e) {
            return response()->json(['success' => false, 'message' => 'File not provided'], 404);
        }

        return response()->json(['success' => true, 'data' => $user]);

    }


    function addUser()
    {

        $data = request()->all();

        if (isset($data['transport'])) {
            $data['transport'] = implode(',', $data['transport']);
        }

        if (!User::checkIban($data['iban'])) {
            return response()->json(['success' => false, 'message'=>'Invalid IBAN'], 500);
        }

        User::create($data);

        $user = User::orderBy('id', 'desc')->first();

        return $user;
    }


    function updateUserChild()
    {
        $data = request()->all();

        $child = Child::find($data['id']);

        $child->update($data);

        $user = User::with('children')->find($child['user_id']);

        return response()->json(['success' => true, 'data' => $user]);
    }


    function addUserChild()
    {
        $data = request()->all();

        Child::create($data);

        $user = User::with('children')->find($data['user_id']);

        return response()->json(['success' => true, 'data' => $user]);
    }


    function deleteUserChild($id)
    {
        $child = Child::find($id);

        $userId = $child['user_id'];

        $child->delete();

        $user = User::with('children')->find($userId);

        return response()->json(['success' => true, 'data' => $user]);
    }


    function getRoleNameForUser ($users)
    {

        foreach ($users as $user) {
            if (!is_null($user['role_id'])) {
                $user['roleName'] = 'Admin';
            }
            else {
                $supervisor = User::where('supervisor_id', '=', $user['id'])->first();
                if (!is_null($supervisor)) {
                    $user['roleName'] = 'Supervisor';
                }
                else {
                    $user['roleName'] = 'User';
                }
            }
        }
        return $users;
    }
}
