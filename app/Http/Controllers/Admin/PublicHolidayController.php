<?php

namespace App\Http\Controllers\Admin;

use App\Models\PublicHoliday;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicHolidayController extends Controller
{
    function getAdminPublicHolidays()
    {
        $publicHolidays = PublicHoliday::getPublicHolidaysList(request());

        $years = (new PublicHoliday)->getYearsList();

        return response()->json(['success' => true, 'data'=> [
                'holidays' =>  $publicHolidays,
                'years' =>  $years,
            ]
        ]);
    }


    function addPublicHolidays()
    {
        PublicHoliday::create( request()->all());

        $publicHolidays = PublicHoliday::getPublicHolidaysList(request());

        return response()->json(['success' => true, 'data'=> $publicHolidays]);
    }


    function updatePublicHolidays()
    {
        $data = request()->all();

        $publicHoliday = PublicHoliday::find($data['id']);

        $publicHoliday->update($data);

        $publicHolidays = PublicHoliday::getPublicHolidaysList(request());

        return response()->json(['success' => true, 'data'=> $publicHolidays]);
    }
}
