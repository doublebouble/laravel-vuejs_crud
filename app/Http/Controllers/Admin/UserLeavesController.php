<?php

namespace App\Http\Controllers\Admin;

use App\Models\LeaveRequest;
use App\Models\PublicHoliday;
use App\Models\User;
use App\Models\UserLeave;
use App\Models\UserPublicHoliday;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserLeavesController extends Controller
{

    function getAdminUsersLeave()
    {
        $requestData = request()->all();

        $years = UserLeave::getYearsListLeave();

        $usersList = User::where('active', '=', 1)->get();

        $publicHolidays = PublicHoliday::getHolidaysCurrentYear($requestData['year'], $requestData['nextYear']);

        $users = User::getUsersLeave($requestData);

        return response()->json([
            'years' => $years,
            'usersList' => $usersList,
            'usersLeave' => $users,
            'publicHolidays' => $publicHolidays
        ]);
    }


    function getAdminUsersLeaveWithFilter()
    {
        $requestData = request()->all();

        $publicHolidays = PublicHoliday::getHolidaysCurrentYear($requestData['year'], $requestData['nextYear']);

        $users = User::getUsersLeave($requestData);

        return response()->json([
            'usersLeave' => $users,
            'publicHolidays' => $publicHolidays
        ]);
    }


    function updateUserLeave()
    {
        $requestData = request()->all();


        $userLeaveId = $requestData['userLeave']['id'];

        $userLeave = UserLeave::find($userLeaveId);
        try {
            $userLeave->update($requestData['userLeave']);

            if(!empty($requestData['notActivePublicHolidayIds'])){
                foreach ($requestData['notActivePublicHolidayIds'] as $notActivePublicHolidayId){
                    UserPublicHoliday::where('user_id', '=', $requestData['userLeave']['user_id'])
                        ->where('public_holiday_id', '=', $notActivePublicHolidayId)
                        ->delete();
                }
            }

            if(!empty($requestData['activePublicHolidayIds'])){
                foreach ($requestData['activePublicHolidayIds'] as $activePublicHolidayId){
                    $createData = [];
                    $createData['user_id'] = $requestData['userLeave']['user_id'];
                    $createData['public_holiday_id'] = $activePublicHolidayId;
                    $publicHolidayInLeave = UserPublicHoliday::whereUserId($requestData['userLeave']['user_id'])->where('public_holiday_id', $activePublicHolidayId)->first();
                    if (is_null($publicHolidayInLeave)) {
                        UserPublicHoliday::create($createData);
                    }
                }
            }

            $publicHolidays = PublicHoliday::getHolidaysCurrentYear($requestData['year'], $requestData['nextYear']);

            $users = User::getUsersLeave($requestData);


            return response()->json([
                'usersLeave' => $users,
                'publicHolidays' => $publicHolidays
            ]);
        }
        catch (Exception $e) {
            return response()->json(['success' => false, 'message'=>$e->getMessage()], 501);
        }

    }


    function createUserLeave()
    {
        $requestData = request()->all();

        $requestData['userLeave']['user_id'] = $requestData['userLeave']['id_user'];

        try {
            UserLeave::create($requestData['userLeave']);

            if(!empty($requestData['notActivePublicHolidayIds'])){
                foreach ($requestData['notActivePublicHolidayIds'] as $notActivePublicHolidayId){
                    UserPublicHoliday::where('user_id', '=', $requestData['userLeave']['id_user'])
                        ->where('public_holiday_id', '=',$notActivePublicHolidayId)
                        ->delete();
                }
            }

            if(!empty($requestData['activePublicHolidayIds'])){
                foreach ($requestData['activePublicHolidayIds'] as $activePublicHolidayId){

                    UserPublicHoliday::create([
                        'user_id' => $requestData['userLeave']['id_user'],
                        'public_holiday_id' => $activePublicHolidayId
                    ]);
                }
            }

            $publicHolidays = PublicHoliday::getHolidaysCurrentYear($requestData['year'], $requestData['nextYear']);

            $users = User::getUsersLeave($requestData);


            return response()->json([
                'usersLeave' => $users,
                'publicHolidays' => $publicHolidays
            ]);
        }
        catch (Exception $e) {
            return response()->json(['success' => false, 'message'=>$e->getMessage()], 501);
        }

    }


    function copyCurYearToNextYearUserLeave()
    {
        $year = Carbon::now()->year;
        $requestData = request()->all();

        $alreadyCopiedUsers = User::getNextYearUsers();

        $usersIns = User::getUsersLeaveForCopyToNextYear($year, $alreadyCopiedUsers);

        if (empty($usersIns)) {
            return 'Already copied';
        }

        $usersIns = array_map(function ($usersLeave) use ($year) {
            $userLeave = (array)$usersLeave;
            $userLeave['year'] = $year+1;
            return $userLeave;
        }, $usersIns);

        DB::table('la_user_leaves')->insert($usersIns);

        $publicHolidays = PublicHoliday::getHolidaysCurrentYear($year, $year+1);

        $users = User::getUsersLeave($requestData);

        $years = UserLeave::getYearsListLeave();

        return response()->json([
            'usersLeave' => $users,
            'publicHolidays' => $publicHolidays,
            'years' => $years
        ]);



    }
}
