<?php

namespace App\Http\Controllers\Admin;

use App\Models\LeaveType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeaveTypeController extends Controller
{
    function getAdminLeavetypes()
    {
        $leavetypes = LeaveType::paginate(15);

        return response()->json(['success' => true, 'data'=> $leavetypes]);
    }


    function addLeavetype()
    {
        LeaveType::create(request()->all());

        $leavetypes = LeaveType::paginate(15);

        return response()->json(['success' => true, 'data'=> $leavetypes]);
    }


    function updateLeavetype()
    {
        $data = request()->all();

        $leavetype = LeaveType::find($data['id']);

        $leavetype->update($data);

        $leavetypes = LeaveType::paginate(15);

        return response()->json(['success' => true, 'data'=> $leavetypes]);
    }
}
