<?php

namespace App\Http\Controllers\Admin;

use App\Mail\AddRequest;
use App\Mail\AdminAddLeave;
use App\Models\LeaveRequest;
use App\Models\LeaveType;
use App\Models\PaycheckProcessed;
use App\Models\User;
use App\Models\UserLeave;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;

class LeaveRequestController extends Controller
{
    function getAdminLeavetypesUsersList()
    {
        $leavetypes = LeaveType::all();
        $users = User::where('active', '=', 1)
            ->select('id', 'name', 'surname')
            ->get();

        return response()->json(['success' => true, 'data'=> [
            'leavetypes' => $leavetypes,
            'users' => $users,
            ]
        ]);
    }


    function addLeave()
    {
        $data = request()->all();

        $admin = JWTAuth::user();

        $data['request_status_id'] = 2;
        $leaveRequest = LeaveRequest::create($data);

        $leaveRequest['nameLeaveType'] = LeaveType::find($leaveRequest['leavetype_id'])->name;

        $userOfRequest = User::getDataForEmailUser($data['user_id']);

        $supervisor = User::getDataForEmailUser($userOfRequest['supervisor_id']);
        $mailData = [
            'user' => $userOfRequest,
            'supervisor' => $supervisor,
            'admin' => $admin,
            'leaveRequest' => $leaveRequest,
            'supervisor_s' => '',
            'admin_s' => ''
        ];

        try {
            $subjectUser = 'Your leave is granted';
            Mail::to($userOfRequest->email)->send(new AdminAddLeave($mailData, $subjectUser));

            if (!is_null($supervisor)) {
                $mailData['supervisor_s'] = 1;
                $subjectsupervisor = $admin->name.' '.$admin->surname.' added and granted leave of '.$userOfRequest->name.' '.$userOfRequest->surname;
                Mail::to($supervisor->email)->send(new AdminAddLeave($mailData, $subjectsupervisor));
            }

            $mailData['admin_s'] = 1;
            $subjectAdmin = 'You added and granted leave of '.$userOfRequest->name.' '.$userOfRequest->surname;
            Mail::to($admin->email)->send(new AdminAddLeave($mailData, $subjectAdmin));

            return response()->json(['success' => true, 'data'=> $leaveRequest]);
        }
        catch (Exception $e) {
            Log::error('ERRORHERE ' . print_r($e,1));

            return response()->json(['success' => false]);
        }
    }


    function getAdminPaycheckProcess()
    {
        $leaveRequests = LeaveRequest::getPaychekProcess();

        return response()->json(['success' => true, 'data'=> $leaveRequests]);
    }


    function addPaycheckProcess()
    {
        $data = request()->all();
        $userId = JWTAuth::user()->id;
        $paycheckProcessed = Carbon::now()->toDateTimeString();

        $paycheckProcess = [
            'leave_request_id' => $data['id'],
            'paycheck_processed_user_id' => $userId,
            'paycheck_processed' => $paycheckProcessed
        ];

        PaycheckProcessed::create($paycheckProcess);

        $leaveRequests = LeaveRequest::getPaychekProcess();

        return response()->json(['success' => true, 'data'=> $leaveRequests]);


    }

}
