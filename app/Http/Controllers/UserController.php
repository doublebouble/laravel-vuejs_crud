<?php

namespace App\Http\Controllers;

use App\Models\Child;
use App\Models\LeaveRequest;
use App\Models\Role;
use App\Models\User;
use App\Models\UserLeave;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserController extends Controller
{

    function showLoggedUserData ()
    {
        $userId = JWTAuth::user()->id;
        $user = User::with('children')->find($userId);
        return response()->json(['success' => true, 'data'=>$user]);
    }


    function updateUserProfile ()
    {
        $data = request()->all();
        $userId = JWTAuth::user()->id;
        $user = User::find($userId);

        if (isset($data['transport'])) {
            $data['transport'] = implode(',', $data['transport']);
        }

        if (!User::checkIban($data['iban'])) {
            return response()->json(['success' => false, 'message'=>'Invalid IBAN'], 500);
        }

        try {
            $user->update($data);

            return response()->json(['success' => true, 'data'=>$user]);
        }
        catch (Exception $e) {
            return response()->json(['success' => false, 'message'=>$e->getMessage()], 501);
        }
    }


    function updateUserProfilePicture(Request $request)
    {
        $userId = JWTAuth::user()->id;
        $user = User::with('children')->find($userId );

        $this->validate($request,[
            'file'=> 'mimes:png,jpeg,jpg|max:1024'
        ]);

        if(!$request->hasFile('profile-picture'))
        {
            return response()->json(['success' => false, 'message' => 'File not provided'], 404);
        }

        try {
            $user->updateProfilePicture($request->file('profile-picture'));
        }
        catch (Exception $e) {
            return response()->json(['success' => false, 'message' => 'File not provided'], 404);
        }


        return response()->json(['success' => true, 'data' => $user]);

    }


    function changeDefaultLang () {
        $data = request()->all();
        $userId = JWTAuth::user()->id;
        $user = User::find($userId);

        try {
            $user->default_lang = $data['default_lang'];
            $user->save();


            $user = User::with('children')->find($userId);

            return response()->json(['success' => true, 'data'=>$user]);
        }
        catch (Exception $e) {
            return response()->json(['success' => false, 'message'=>$e->getMessage()], 501);
        }

    }
}
