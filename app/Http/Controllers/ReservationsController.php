<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReservationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = request()->all();

        $reservationsQuery = Reservation::with('customer');

        if(isset($params['room_id[]'])) {
            $reservationsQuery->where('room_id', 'in', $params['room_id[]']);
        }

        if(isset($params['customer_first_name'])) {
            $reservationsQuery->whereHas('customer', function ($q) use ($params) {
                $q->where('first_name', 'like', "'%$params[customer_first_name]%'");
            });
        }

        if(isset($params['customer_last_name'])) {
            $reservationsQuery->whereHas('customer', function ($q) use ($params) {
                $q->where('last_name', 'like', "'%$params[customer_last_name]%'");
            });
        }

        if(isset($params['time_from'])) {
            $reservationsQuery->where('time_from', '>=' ,$params['time_from']);
        }

        if(isset($params['time_till'])) {
            $reservationsQuery->where('time_till', '<=' ,$params['time_till']);
        }

        //TODO: clarify what is the "cleaning_ready_stamp" ?
        if(isset($params['cleaning_ready_stamp'])) {
            $reservationsQuery->where('cleaning_ready_stamp',$params['cleaning_ready_stamp']);
        }

        if(isset($params['amount_persons_from'])) {
            $reservationsQuery->where('amount_persons','>=',$params['amount_persons_from']);
        }

        if(isset($params['amount_persons_to'])) {
            $reservationsQuery->where('amount_persons','<=',$params['amount_persons_to']);
        }

        if(isset($params['calculated_price_from'])) {
            $reservationsQuery->where('calculated_price','>=',$params['calculated_price_from']);
        }

        if(isset($params['calculated_price_to'])) {
            $reservationsQuery->where('calculated_price','<=',$params['calculated_price_to']);
        }

        if(isset($params['shown_up'])) {
            $reservationsQuery->where('shown_up',$params['shown_up']);
        }

        $reservations = $reservationsQuery->paginate(20);

        return response($reservations);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO: for Julia. example of JS that generates correct format for time_from and time_till:
        // moment('2018-11-16 12:00:00').utc().format();

        $data = $request->validate(Reservation::getValidationRules());

        $isValidDuration = Reservation::checkForValidDuration($data['time_from'], $data['time_till']);

        if(!$isValidDuration) {
            return response()->json('Provided period is not valid',501);
        }

        $data['customer_id'] = Auth::user()->id;

        $reservation = Reservation::create($data);

        return response()->json($reservation);
    }


    public function storeByAdmin(Request $request)
    {
        //TODO: for Julia. example of JS that generates correct format for time_from and time_till:
        // moment('2018-11-16 12:00:00').utc().format();

        $data = $request->validate(Reservation::getValidationRulesForAdmin());

        $isValidDuration = Reservation::checkForValidDuration($data['time_from'], $data['time_till']);

        if(!$isValidDuration) {
            return response()->json('Provided period is not valid',501);
        }

        $reservation = Reservation::create($data);

        return response()->json($reservation);
    }


    public function updateByAdmin(Request $request, Reservation $reservation)
    {
        $data = $request->validate(Reservation::getValidationRulesForAdmin());

        $isValidDuration = Reservation::checkForValidDuration($data['time_from'], $data['time_till']);

        if(!$isValidDuration) {
            return response()->json('Provided period is not valid',501);
        }

        $reservation->update($data);

        return response()->json($reservation);
    }


    /**
     * Display the specified resource.
     *
     * @param Reservation $reservation
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Reservation $reservation)
    {
        return response()->json($reservation);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Reservation $reservation
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Reservation $reservation)
    {
        if(!Auth::user()->is_admin) {
            return response()->json('allowed for admin only', 403);
        }

        $reservation->delete();

        return response()->json('ok');
    }
}
