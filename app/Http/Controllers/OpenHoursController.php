<?php

namespace App\Http\Controllers;

use App\Models\OpenHour;
use Illuminate\Http\Request;

class OpenHoursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $openHours = OpenHour::all();

        return response()->json($openHours);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate(OpenHour::getValidationRules());

        $openHour = OpenHour::create($data);

        return response()->json($openHour);
    }

    /**
     * Display the specified resource.
     *
     * @param OpenHour $openHour
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(OpenHour $openHour)
    {
        return response()->json($openHour);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param OpenHour $openHour
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, OpenHour $openHour)
    {
        $data = $request->validate(OpenHour::getValidationRules());

        $openHour = $openHour->update($data);

        return response()->json($openHour);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param OpenHour $openHour
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(OpenHour $openHour)
    {
        $openHour->delete();

        return response()->json('ok');
    }
}
