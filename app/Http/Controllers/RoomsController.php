<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoomRequest;
use App\Http\Requests\UpdateRoomRequest;
use App\Models\Room;
use App\Services\CrudService;
use Illuminate\Http\Request;

class RoomsController extends Controller
{
    const PER_PAGE = 20;

    protected $crudService;

    public function __construct()
    {
        $this->crudService = new CrudService(new Room());
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roomsQuery = $this->crudService->index($request);

        $rooms = $roomsQuery->paginate(self::PER_PAGE);

        return response()->json($rooms);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRoomRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoomRequest $request)
    {
        $roomsQuery = $this->crudService->store($request);

        $rooms = $roomsQuery->paginate(self::PER_PAGE);

        return response()->json($rooms);
    }


    /**
     * Display the specified resource.
     *
     * @param Room $room
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Room $room)
    {
        return response()->json($this->crudService->show($room));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRoomRequest $request
     * @param Room $room
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(UpdateRoomRequest $request, Room $room)
    {
        $roomsQuery = $this->crudService->update($request, $room);

        $rooms = $roomsQuery->paginate(self::PER_PAGE);

        return response()->json($rooms);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Room $room
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Request $request, Room $room)
    {
        $roomsQuery = $this->crudService->destroy($request, $room);

        $rooms = $roomsQuery->paginate(self::PER_PAGE);

        return response()->json($rooms);
    }
}
