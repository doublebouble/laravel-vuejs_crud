<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\Customer;
use App\Services\CrudService;
use Illuminate\Http\Request;

class CustomersController extends Controller
{

    const PER_PAGE = 20;

    protected $crudService;

    public function __construct()
    {
        $this->crudService = new CrudService(new Customer());
    }


    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customersQuery = $this->crudService->index($request);

        $customers = $customersQuery->paginate(self::PER_PAGE);

        return response()->json($customers);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCustomerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCustomerRequest $request)
    {
        $customersQuery = $this->crudService->store($request);

        $customers = $customersQuery->paginate(self::PER_PAGE);

        return response()->json($customers);
    }


    /**
     * Display the specified resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Customer $customer)
    {
        return response()->json($this->crudService->show($customer));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCustomerRequest $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(UpdateCustomerRequest $request, Customer $customer)
    {
        $customersQuery = $this->crudService->update($request, $customer);

        $customers = $customersQuery->paginate(self::PER_PAGE);

        return response()->json($customers);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Request $request, Customer $customer)
    {
        $customersQuery = $this->crudService->destroy($request, $customer);

        $customers = $customersQuery->paginate(self::PER_PAGE);

        return response()->json($customers);
    }
}
