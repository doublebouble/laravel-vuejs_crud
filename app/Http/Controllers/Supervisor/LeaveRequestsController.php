<?php

namespace App\Http\Controllers\Supervisor;

use App\Mail\StatusRequest;
use App\Models\LeaveRequest;
use App\Models\LeaveType;
use App\Models\PublicHoliday;
use App\Models\User;
use App\Models\UserLeave;
use App\Models\UserPublicHoliday;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;
use Mockery\Exception;

class LeaveRequestsController extends Controller
{

    function getSupervisorOverview ()
    {
        $requestData = request()->all();
        $leaveDays  = UserLeave::getLeaveDays($requestData['year'], $requestData['nextYear']);

        $requestedLeaves = LeaveRequest::getRequestedLeaves($requestData['year'], $requestData['nextYear']);

        $years = LeaveRequest::getYearsListLeaveRequest();

        return response()->json(['success' => true, 'data'=>[
            'leaveDays' => $leaveDays,
            'requestedLeaves' => $requestedLeaves,
            'years' => $years
        ]
        ]);
    }


    function getSupervisorLeaveDaysPage ()
    {
        $requestData = request()->all();

        $leaveDays  = UserLeave::getLeaveDays( $requestData['year'], $requestData['nextYear']);

        return response()->json($leaveDays);
    }


    function getSupervisorRequestedLeavesPage ()
    {
        $requestData = request()->all();

        $requestedLeaves  = LeaveRequest::getRequestedLeaves($requestData['year'], $requestData['nextYear']);

        return response()->json($requestedLeaves);
    }


    function getLeaveDaysUser ()
    {
        $requestData = request()->all();

        $publicHolidays = PublicHoliday::getPublicHolidaysForUser($requestData['userId'], $requestData['year'], $requestData['nextYear']);

        $takenDays = UserLeave::getTakenDays($requestData['userId'], $requestData['year'], $requestData['nextYear']);

        $totalTaken = LeaveRequest::getTotalTaken($requestData['userId'], $requestData['year'], $requestData['nextYear']);

        $groupLeave["type"] = LeaveType::all();

        foreach ($groupLeave["type"] as $key => $value){
            $groupLeave["type"]["$key"]["leaveRequest"] = LeaveRequest::where('user_id', $requestData['userId'])
                ->where('leavetype_id', $value['id'])
                ->where('start_date','>=', $requestData['year'])
                ->where('start_date','<', $requestData['nextYear'])
                ->where('request_status_id','<', 3)
                ->get();
        }

        $userInfo = User::where('id', '=', $requestData['userId'])
            ->first([
                'id',
                'name',
                'surname',
            ]);

        return response()->json([
            'takenDays' => $takenDays,
            'totalTaken' => $totalTaken,
            'publicHolidays' => $publicHolidays,
            'groupLeave' => $groupLeave,
            'userInfo' => $userInfo,
        ]);
    }


    function updateStatusRequest ()
    {
        $requestData = request()->all();

        $supervisor = JWTAuth::user();

        if(empty($requestData['reason'])){
            $requestData['reason'] = null;
            $requestData['request_status'] = 2;
            $status = 'granted';
        }else {
            $requestData['request_status'] = 3;
            $status  = 'denied';
        }

        LeaveRequest::updateLeaveRequest($requestData['id'], $requestData['request_status'], $supervisor['id'], $requestData['reason']);

        $requestedLeaves  = LeaveRequest::getRequestedLeaves($requestData['year'], $requestData['nextYear']);

        $userOfRequest = User::getDataForEmailUser($requestData['user_id']);

        $mailData = [
            'user' => $supervisor,
            'userOfRequest' => $userOfRequest,
            'leaveRequest' => $requestData,
            'status' => $status,
            'supervisor' => ''
        ];

        try {
            $subjectUserMail = 'Status of your leave request';
            Mail::to($userOfRequest->email)->send(new StatusRequest($mailData, $subjectUserMail));

            $mailData['supervisor'] = 1;

            $subjectsupervisorMail = 'Status of '.$userOfRequest['name'].' '. $userOfRequest['surname'].' leave request';
            Mail::to($supervisor->email)->send(new StatusRequest($mailData, $subjectsupervisorMail));

            return response()->json($requestedLeaves);
        }
        catch (Exception $e) {
            Log::error('ERRORHERE ' . print_r($e,1));

            return response()->json(['success' => false]);
        }


    }
}
