<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotASupervisor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->isSupervisor()) {
            return response()->json('invalide role', 403);
        }

        return $next($request);
    }
}
