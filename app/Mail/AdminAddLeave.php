<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminAddLeave extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $emailData;
    public $subject;

    public function __construct($data, $subj)
    {
        $this->emailData = $data;
        $this->subject = $subj;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@sporta.be', 'Sporta Leave')
            ->subject($this->subject)
            ->markdown('emails.edit-request');
    }
}
