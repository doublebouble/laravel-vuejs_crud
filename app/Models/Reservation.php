<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservations';

    protected $fillable = [
        'room_id',
        'customer_id',
        'time_from',
        'time_till',
        'cleaning_ready_stamp',
        'amount_persons',
        'calculated_price',
        'shown_up',
        'memo',
    ];


    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }


    static function getValidationRules()
    {
        return [
            'room_id' => 'required|integer',
            'time_from' => 'required|date',
            'time_till' => 'required|date ',
            'amount_persons' => 'integer|min:2',
        ];
    }


    static function getValidationRulesForAdmin()
    {
        return [
            'room_id' => 'required|integer',
            'customer_id' => 'required|integer',
            'time_from' => 'required|date',
            'time_till' => 'required|date ',
            'amount_persons' => 'integer|min:2',
            'memo' => 'string|max:1000'
        ];
    }


    static function checkForValidDuration($timeFrom, $timeTill)
    {
        $from = Carbon::parse($timeFrom);
        $till = Carbon::parse($timeTill);

        if($from < Carbon::now('UTC')) {
            return false;
        }

        $duration = $till->diffInHours($from);

        return $duration >= 3 && $duration <= 12;
    }


    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->time_from = Carbon::parse($model->time_from);
            $model->time_till = Carbon::parse($model->time_till);
            $model->cleaning_ready_stamp = $model->time_till->addMinutes(30);
        });

        self::created(function($model){
            // ... code here
        });

        self::updating(function($model){
            $model->time_from = Carbon::parse($model->time_from);
            $model->time_till = Carbon::parse($model->time_till);
            $model->cleaning_ready_stamp = $model->time_till->addMinutes(30);
        });

        self::updated(function($model){
            // ... code here
        });

        self::deleting(function($model){
            // ... code here
        });

        self::deleted(function($model){
            // ... code here
        });
    }
}
