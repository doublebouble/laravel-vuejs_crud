<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageWord extends Model
{
    protected $table = 'language_words';

    protected $guarded = ['id'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

}
