<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class OpenHour extends Model
{
    protected $table = 'open_hours';

    protected $fillable = [
        'weekday',
        'from_time',
        'till_time',
        'closed'
    ];

    static $weekDays = [
        'Mon',
        'Tue',
        'Wen',
        'Thu',
        'Fri',
        'Sat',
        'Sun'
    ];

    static function getValidationRules (){
        return [
            'weekday' => ['required', Rule::in([self::$weekDays])],
            'from_time' => 'required|date_format:H:i',
            'till_time' => 'required|date_format:H:i',
        ];
    }
}
