<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Facades\JWTAuth;
use GuzzleHttp\Client;

class User extends Authenticatable implements JWTSubject
{

    use Notifiable;

    protected $fillable = [
        'email',
        'password',
        'is_verified',
        'remember_token',
        'name',
        'default_lang',
    ];

    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['birth_date', 'created_at', 'updated_at', 'deleted_at'];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function getAvatarAttribute($value)
    {
        return env('APP_URL') . '/storage/' . $value;
    }


    function updateProfilePicture(UploadedFile $file)
    {
        $filePath = Storage::disk('public')->putFile('profile-pictures', $file);

        $this->avatar = $filePath;

        $this->save();
    }


//    function children()
//    {
//        return $this->hasMany(Child::class);
//    }
//
//
//    function leaveRequests()
//    {
//        return $this->hasMany(LeaveRequest::class);
//    }
//
//
//    function userLeaves()
//    {
//        return $this->hasMany(UserLeave::class);
//    }
//
//
//    function userRole()
//    {
//        return $this->hasOne(UserRole::class);
//    }
//
//
//    function userPublicHolidays()
//    {
//        return $this->hasMany(UserPublicHoliday::class);
//    }


    static function getDataForEmailUser($userId)
    {
        return self::where('id', '=', $userId)
            ->select([
                'email',
                'surname',
                'name',
                'supervisor_id'
            ])
            ->first();
    }


    static function getUsers($request){

        $users = User::with('children')
            ->with(['userPublicHolidays'=>function($query) use ($request){
                $query->join('la_public_holidays', 'la_public_holidays.id', '=', 'la_user_public_holidays.public_holiday_id')
                    ->whereRaw('year(la_public_holidays.date)>='.$request['year'].' AND year(la_public_holidays.date)<'.$request['nextYear']);
            }])
            ->where('users.deleted_at', Null)

            ->leftJoin('la_user_leaves', function($join) use ($request){
                $join->on('la_user_leaves.user_id', '=', 'users.id')
                    ->where('la_user_leaves.year', '>=', $request['year'])
                    ->where('la_user_leaves.year', '<', $request['nextYear']);
            });
        $users = self::composeConditionsStatusUser($users, $request);

        $users = self::composeConditionsUser($users, $request);

        $users = self::composeConditionsSupervisor($users, $request);

        $users = $users->leftJoin('user_role', 'user_role.user_id', '=', 'users.id')
            ->leftJoin('roles', 'user_role.role_id', '=', 'roles.id')
            ->selectRaw('      
            role_id,
            roles.name as roleName,      
            default_leavedays,
            extra_leavedays,
            extra_leavedays_teamleader,
            default_leavehours,
            old_overtime,
            users.*,
            users.name as name,
            users.email as email,
            users.id as id,
            users.surname as surname
            ')
            ->orderBy($request['sortName'], $request['sortParam'])
            ->paginate($request['quantityUser']);

        return $users;
    }

    public static function composeConditionsStatusUser($query, $request)
    {
        if (isset($request['activeUser'])) {
            return $query->where('users.active', '=', (int)$request['activeUser']);
        }
    }


    static function composeConditionsUser($query, $request)
    {
        if (isset($request['users']) && !empty($request['users'])) {
            $query->whereIn('users.id', $request['users']);
        }
        return $query;
    }


    static function composeConditionsSupervisor($query, $request)
    {
        if (isset($request['supervisor']) && !empty($request['supervisor'])) {
            $query->whereIn('users.supervisor_id', $request['supervisor']);
        }
        return $query;
    }


    static function getAllSupervisors ()
    {
        $supervisorIds = User::where('supervisor_id', '>', 0)->get()->pluck('supervisor_id');

        return User::whereIn('id', $supervisorIds)
            ->select([
                'users.id',
                'users.name',
                'users.surname',
                'users.email'
            ])
            ->orderBy('name')
            ->get();
    }


    static function getAllSupervisorsModal ()
    {
        return User::select([
                'users.id',
                'users.name',
                'users.surname',
                'users.email'
            ])
            ->orderBy('name')
            ->get();
    }


    static function getUsersLeave($request)
    {

        $users = DB::table('users')
            ->selectRaw('la_user_leaves.*, 
                        users.name as name, 
                        users.surname as surname, 
                        users.id as id_user, 
                        GROUP_CONCAT(la_user_public_holidays.public_holiday_id SEPARATOR ", ") AS public_holiday_ids,
                        COUNT(la_user_public_holidays.public_holiday_id) AS holiday_number')
            ->leftJoin('la_user_public_holidays',function($join) use ($request){
                $join->on('users.id', '=', 'la_user_public_holidays.user_id')
                    ->whereRaw('la_user_public_holidays.public_holiday_id 
	                IN (SELECT la_public_holidays.id FROM la_public_holidays WHERE YEAR(date)>= '.$request['year'].' AND YEAR(date)<'.$request['nextYear'].')');
            } )
            ->leftJoin('la_user_leaves', function($join) use ($request){
                $join->on('la_user_leaves.user_id', '=', 'users.id')
                    ->where('la_user_leaves.year', '>=', $request['year'])
                    ->where('la_user_leaves.year', '<', $request['nextYear']);
            });


        $users = self::composeConditionsUser($users, $request);

        $users = $users->whereNull('users.deleted_at')
            ->where('users.active', '=', 1)
            ->groupBy('users.id')
            ->paginate(15);

        return $users;
    }


    static function getUsersLeaveForCopyToNextYear($year, $alreadyCopiedUsers)
    {
        $users = DB::table('users')
            ->select(['la_user_leaves.user_id',
                'la_user_leaves.year',
                'la_user_leaves.default_leavedays',
                'la_user_leaves.extra_leavedays',
                'la_user_leaves.extra_leavedays_teamleader',
                'la_user_leaves.default_leavehours',
                'la_user_leaves.old_overtime'
            ])
            ->join('la_user_leaves', 'la_user_leaves.user_id', '=', 'users.id')
            ->where('la_user_leaves.year', '=', $year)
            ->whereNull('users.deleted_at')
            ->where('users.active', '=', 1)
            ->whereNotIn('la_user_leaves.user_id', $alreadyCopiedUsers)
            ->get()
            ->toArray();

        return $users;
    }


    static function getNextYearUsers()
    {
        $year = Carbon::now()->year;

        return DB::table('users')
            ->select('la_user_leaves.user_id')
            ->join('la_user_leaves', 'la_user_leaves.user_id', '=', 'users.id')
            ->whereNull('users.deleted_at')
            ->where('users.active', '=', 1)
            ->where('la_user_leaves.year', '=', $year+1)
            ->get()
            ->pluck('user_id')
            ->toArray();
    }


    function isAdmin()
    {
        $userId = JWTAuth::user()->id;

        $userRole = UserRole::select('role_id')->where('user_id', '=', $userId)->first();

        if ($userRole['role_id'] <= 1) {
            return true;
        }
        return false;
    }


    function isSupervisor()
    {
        $userId = JWTAuth::user()->id;

        $supervisor = User::where('supervisor_id', '=', $userId)->first();

        if (!is_null($supervisor)) {
            return true;
        }
        else {
            $userRole = UserRole::select('role_id')->where('user_id', '=', $userId)->first(); //not supervisor, but admin
            if ($userRole['role_id'] <= 1) {
                return true;
            }
        }
        return false;
    }


    static function checkIban ($code)
    {
        $http = new Client;

        $applicationId = '3e51c0e6';
        $key = 'dddafe7f5816ce9a1309f4f990ba5ccb';
        $url = 'https://api.tuxx.co.uk/1.0/finance/iban/validate.php?value='.$code.'&language=en&app_id='.$applicationId.'&app_key='.$key;
        $response = $http->get($url);
        $stringResponse = json_decode((string)$response->getBody());

        return $stringResponse->result;
    }
}

