<?php

namespace App\Models;

use App\Services\Contracts\Filterable;
use Illuminate\Database\Eloquent\Model;

class Room extends Model implements Filterable
{
    protected $table = 'rooms';

    protected $fillable = [
        'name',
        'price_per_hour',
        'price_30minutes_extra',
        'price_person_extra',
        'min_hours',
        'max_hours',
        'min_persons',
        'max_persons',
        'cleanup_time_minutes',
        'base_price_amount_persons',
    ];

    public static function getValidationRules()
    {
        return [
            'name' => 'required|max:255',
            'price_per_hour' => 'required',
            'price_30minutes_extra' => 'nullable',
            'price_person_extra' => 'nullable',
            'min_hours' => 'required',
            'max_hours' => 'required',
            'min_persons' => 'required',
            'max_persons' => 'required',
            'cleanup_time_minutes' => 'nullable',
            'base_price_amount_persons' => 'nullable'
        ];
    }

    public function getFilterableAttributes(): array
    {
        return [
            'name' => 'like',
            'price_per_hour' => '=',
            'price_30minutes_extra' => '=',
            'price_person_extra' => '=',
            'min_hours' => '=',
            'max_hours' => '=',
            'min_persons' => '=',
            'max_persons' => '=',
            'cleanup_time_minutes' => '=',
            'base_price_amount_persons' => '=',
        ];
    }
}
