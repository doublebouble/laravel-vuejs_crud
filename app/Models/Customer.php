<?php

namespace App\Models;

use App\Services\Contracts\Filterable;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model implements Filterable
{
    protected $table = 'customers';

    protected $fillable = [
        'first_name',
        'last_name',
        'address',
        'phone',
        'email'
    ];

    public static function getValidationRules(): array
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'address' => 'max:255',
            'phone' => 'required|unique:customers|max:255',
            'email' => 'required|unique:customers|max:255|email'
        ];
    }

    public function getFilterableAttributes(): array
    {
        return [
            'first_name' => 'like',
            'last_name' => 'like',
            'address' => 'like',
            'phone' => 'like',
            'email' => 'like'
        ];
    }
}
