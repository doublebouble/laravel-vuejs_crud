<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationUpsell extends Model
{
    protected $table = 'reservation_upsells';
}
