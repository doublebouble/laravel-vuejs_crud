<?php

namespace App\Services\Contracts;

interface Filterable
{
    /**
     * @return array with names of attributes that can be filtered
     */
    public function getFilterableAttributes(): array;
}