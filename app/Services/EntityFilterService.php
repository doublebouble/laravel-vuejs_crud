<?php

namespace App\Services;

use App\Services\Contracts\Filterable;
use Illuminate\Database\Eloquent\Builder;

class EntityFilterService
{
    protected $query;
    protected $filterable;

    public function __construct(Builder $query, Filterable $filterable)
    {
        $this->query = $query;
        $this->filterable = $filterable;
    }

    public function applyFilter(array $values): Builder
    {
        $filterableAttributes = $this->filterable->getFilterableAttributes();
        $filter = [];
        foreach ($filterableAttributes as $attribute => $operator) {
            if (!empty($values[$attribute])) {
                $filter[] = [
                    $attribute, $operator, ($operator === 'like') ? "%$values[$attribute]%" : $values[$attribute]
                ];
            }
        }
        if (!empty($filter)) {
            return $this->query->where($filter);
        } else {
            return $this->query;
        }

    }
}
