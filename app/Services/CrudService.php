<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CrudService
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function index(Request $request): Builder
    {
        $params = $request->all();

        $filterService = new EntityFilterService($this->model->query(), $this->model);
        $entityQuery = $filterService->applyFilter($params);

        if(isset($params['order-by-field']) && isset($params['order-by-direction'])) {
            $entityQuery = $entityQuery->orderBy($params['order-by-field'], $params['order-by-direction']);
        }

        return $entityQuery;
    }

    public function store(Request $request): Builder
    {
        $data = array_only($request->all(), $this->model->getFillable());

        $this->model->create($data);

        return $this->index($request);
    }

    public function show(Model $model)
    {
        return $model;
    }

    public function update(Request $request, Model $model): Builder
    {
        $data = array_only($request->all(), $this->model->getFillable());

        $model->update($data);

        return $this->index($request);
    }

    public function destroy(Request $request, Model $model): Builder
    {
        $model->delete();

        return $this->index($request);
    }
}