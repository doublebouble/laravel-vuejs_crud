<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOpenHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('weekday', [
                'Mon',
                'Tue',
                'Wen',
                'Thu',
                'Fri',
                'Sat',
                'Sun',
            ])->nullable();
            $table->time('from_time')->nullable();
            $table->time('till_time')->nullable();
            $table->boolean('closed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_hours');
    }
}
