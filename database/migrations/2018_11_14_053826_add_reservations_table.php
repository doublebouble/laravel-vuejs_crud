<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id');
            $table->integer('customer_id');
            $table->timestamp('time_from')->nullable();
            $table->timestamp('time_till')->nullable();
            $table->timestamp('cleaning_ready_stamp')->nullable();
            $table->integer('amount_persons')->nullable();
            $table->float('calculated_price')->nullable();
            $table->boolean('shown_up')->nullable();
            $table->text('memo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
