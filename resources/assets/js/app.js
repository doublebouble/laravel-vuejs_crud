require('./bootstrap');
import router from './routes';
import VueRouter from 'vue-router';
import AuthService from './services/auth-service';
import {Pagination} from 'vue-pagination-2';
import VueTimepicker from 'vue2-timepicker'
import vSelect from 'vue-select'

import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';
import FullCalendar from 'vue-full-calendar';
import "fullcalendar/dist/fullcalendar.min.css";


window.Vue = require('vue');

window.Vue.use(VueRouter);

AuthService.defineIsLoggedIn();

/* Components Registration */
Vue.component('main-component', require('./main-component'));
Vue.component('public-outlet', require('./public-components/public-outlet'));
Vue.component('private-outlet', require('./private-components/private-outlet'));
Vue.component('loader', require('./partial-view-components/loader'));
Vue.component('main-header', require('./private-components/layouts/main-header'));
Vue.component('main-sidebar', require('./private-components/layouts/main-sidebar'));
Vue.component('main-footer', require('./private-components/layouts/main-footer'));
Vue.component('pagination', Pagination);
Vue.component('vue-timepicker', VueTimepicker);
Vue.component('v-select', vSelect);

Vue.prototype.$eventGlobal = new Vue(); // Global event bus

Vue.use(VueInternationalization);
Vue.use(FullCalendar);

const lang = document.documentElement.lang.substr(0, 2);
// or however you determine your current app locale

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});
const app = new Vue({
    el: '#app',
    i18n,
    router: router,
});