const alertifyAlerts = {

    alertSuccess: function (title, message, callback) {
        alertify.alert()
            .setting({
                title: `<h3 class="text-success"><i class="fa fa-check"></i> ${title}</h3>`,
                message: `<p>${message}</p>`,
                onok: callback
            })
            .show(true, 'success-alert-modal');
    },


    alertWarning: function (title, message, callback) {
        alertify.alert()
            .setting({
                title: `<h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> ${title}</h3>`,
                message: `<p>${message}</p>`,
                onok: callback
            })
            .show(true, 'warning-alert-modal');
    },


    alertDanger: function (title, message, callback) {
        alertify.alert()
            .setting({
                title: `<h3 class="text-danger"><i class="fa fa-exclamation-circle"></i> ${title}</h3>`,
                message: `<p>${message}</p>`,
                onok: callback
            })
            .show(true, 'danger-alert-modal');
    },
};

export default alertifyAlerts;