import AuthService from '../services/auth-service';

const http = {
    methods: {

        showLoader: function () {
            $('.spin-loader').removeClass('hidden');
        },


        hideLoader: function () {
            $('.spin-loader').addClass('hidden');
        },


        httpGet: function (url) {
            this.addAuthHeader();
            this.showLoader();
            return axios.get(url)
                .then(data => {
                    this.storeAuthHeader(data.headers);
                    this.hideLoader();
                    return data.data;
                })
                .catch(this.handleErrorResponse)
        },


        httpPost: function (url, data) {
            this.addAuthHeader();
            this.showLoader();
            return axios.post(url, data)
                .then(data => {
                    this.storeAuthHeader(data.headers);
                    this.hideLoader();
                    return data.data;
                })
                .catch(this.handleErrorResponse)
        },


        httpPut: function (url, data) {
            this.addAuthHeader();
            this.showLoader();
            return axios.put(url, data)
                .then(data => {
                    this.storeAuthHeader(data.headers);
                    this.hideLoader();
                    return data.data;
                })
                .catch(this.handleErrorResponse)
        },


        httpDelete: function (url) {
            this.addAuthHeader();
            this.showLoader();
            return axios.delete(url)
                .then(data => {
                    this.storeAuthHeader(data.headers);
                    this.hideLoader();
                    return data.data;
                })
                .catch(this.handleErrorResponse)
        },


        httpPostFile: function(url, fileToUpload, fileKeyName) {
            this.addAuthHeader();

            const formData = new FormData();

            formData.append(fileKeyName || 'file', fileToUpload, fileToUpload.name);

            return axios.post(url, formData)
                .then(data => {
                    this.hideLoader();
                    return data.data;
                })
                .catch(this.handleErrorResponse)
        },


        addAuthHeader: function () {
            if(localStorage.hasOwnProperty('auth-token'))
            {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('auth-token');
            }
            else {
                delete axios.defaults.headers.common['Authorization'];
            }
        },


        storeAuthHeader: function (headers) {
            if(headers.hasOwnProperty('authorization')) {
                localStorage.setItem('auth-token', headers.authorization.replace('Bearer ', ''));
            }
        },


        handleErrorResponse: function (err) {
            this.hideLoader();
            console.log('Handled Error ', err.response);

            if(err.response && err.response.status == 401) {
                AuthService.logout(true);
            }

            if (err.response && err.response.status == 403) {
                this.$router.push('/not-enough-permissions?error=403')
            }

            throw err.response;
        }
    }
};


 export default http;