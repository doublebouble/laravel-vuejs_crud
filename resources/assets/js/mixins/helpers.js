const helpers = {
    allowNumbersOnly: function() {
        $(document).ready(function() {
            $(".allow-numbers-only").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl/cmd+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl/cmd+C
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl/cmd+X
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    },

    getArrayEvents: function (data, idUser){
        let evs = [];
        let background = '';
        let border = '';
        let name = '';
        let userId = idUser;
        $.each(data,function(index,value){
            if(value.user_id == userId){
                if(value.request_status_id == 1) {
                    background = '#f39c12'; //orange
                    border = '#f39c12';
                } else if(value.request_status_id == 2) {
                    background = '#00a65a'; //green
                    border = '#00a65a';

                }
                name = value.leave_type.name + ' with ' + value.start_date;
            } else {
                if(value.request_status_id == 1) {
                    background = '#b5bbc8'; //gray light
                    border = '#b5bbc8';
                } else if(value.request_status_id == 2) {
                    background = '#7f7f80'; //gray
                    border = '#7f7f80';

                }
                name = value.user.name + ' ' + value.user.surname;
            }
            evs.push({
                title  : name,
                start  : value.start_date,
                end    : moment(value.end_date).add(1, 'days').format('YYYY-MM-DD'),
                backgroundColor: background,
                borderColor    : border,
            })
        });
        return evs;
    },
}

export default helpers;