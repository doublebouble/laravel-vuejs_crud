const alertifyConfirms = {

    confirmSuccess: function (title, message, okCallback, cancelCallback) {
        alertify.confirm()
            .setting({
                title: `<h3 class="text-success"><i class="fa fa-check"></i> ${title}</h3>`,
                message: `<p>${message}</p>`,
                onok: okCallback,
                oncancel: cancelCallback
            })
            .show(true, 'success-confirm-modal');
    },


    confirmWarning: function (title, message, okCallback, cancelCallback) {
        alertify.confirm()
            .setting({
                title: `<h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> ${title}</h3>`,
                message: `<p>${message}</p>`,
                onok: okCallback,
                oncancel: cancelCallback
            })
            .show(true, 'warning-confirm-modal');
    },


    confirmDanger: function (title, message, okCallback, cancelCallback) {
        alertify.confirm()
            .setting({
                title: `<h3 class="text-danger"><i class="fa fa-exclamation-circle"></i> ${title}</h3>`,
                message: `<p>${message}</p>`,
                onok: okCallback,
                oncancel: cancelCallback
            })
            .show(true, 'danger-confirm-modal');
    },
};

export default alertifyConfirms;