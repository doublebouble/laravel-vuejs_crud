export default {
    "en": {
        "total": {
            "main_navigation": "Main Navigation",
            "user_menu": "User Menu",
            "dashboard": "Dashboard",
            "leave_overview": "Leave Overview",
            "absences_today": "Absences Today",
            "supervisor_menu": "Supervisor Menu",
            "supervisor_overview": "Supervisor Overview",
            "admin_menu": "Admin menu",
            "users": "Users",
            "leavetypes": "Leavetypes",
            "set_leavedays": "Set Leavedays",
            "public_holiday": "Public Holiday",
            "paycheck_process": "Paycheck process",
            "add_request": "Add Request",
            "add_leave": "Add Leave",
            "absences_overview": "Absences Overview",
            "previous_day": "Previous day",
            "next_day": "Next day",
            "no_data": "No data",
            "back_on": "Back on",
            "type": "Type",
            "start_date": "Start date",
            "end_date": "End date",
            "number_of_days": "Number of Days",
            "start_of_hour": "Start of Hour",
            "number_of_hours": "Number of Hours",
            "send_for_approval": "Send for approval",
            "update_leave_request": "Update Leave Request",
            "save_and_confirm": "Save and confirm",
            "close": "Close",
            "user": "User",
            "edit_request": "Edit Request",
            "welcome": "Welcome",
            "upcoming_confirmed_leaves": "Upcoming confirmed leaves",
            "no_confirmed_leaves": "No confirmed leaves",
            "days_ago": "days ago",
            "start_in": "start in",
            "days": "Days",
            "leaves_awaiting_approval": "Leaves awaiting approval",
            "no_leaves_awaiting_approval": "Leaves awaiting approval",
            "leave_requests": "Leave requests",
            "start_hour": "Start Hour",
            "hours": "Hours",
            "status": "Status",
            "edit": "Edit",
            "remove": "Remove",
            "overview": "Overview",
            "given": "Given",
            "leave_days": "Leave days",
            "extra_leave_days": "Extra Leave Days",
            "public_holidays": "Public Holidays",
            "taken_days": "Taken Days",
            "total_days": "Total Days",
            "total_taken": "Total Taken",
            "total_to_take": "Total To Take",
            "taken_hours": "Taken Hours",
            "total_hours": "Total Hours",
            "days_small": "days",
            "day_small": "day",
            "on": "on",
            "leavetype": "Leavetype",
            "user_selectable": "User selectable",
            "in_hours": "In Hours",
            "add_leavetype": "Add Leavetype",
            "edit_leavetype": "Edit Leavetype",
            "leavetype_name": "Leavetype Name",
            "save_leavetype": "Save Leavetype",
            "update_leavetype": "Update Leavetype",
            "text_desc": "Add\/Edit leavedays for users in",
            "show_processed": "Show Processed",
            "processed": "Processed",
            "mark_done": "Mark Done",
            "profile": "Profile",
            "set_you_profile_settings": "set you profile settings",
            "upload_new_avatar": "Upload New Avatar",
            "surname": "Surname",
            "name": "Name",
            "street": "Street",
            "number": "Number",
            "box": "Box",
            "postal_code": "Postal Code",
            "city": "City",
            "country": "Country",
            "gender": "Gender",
            "nationality": "Nationality",
            "birth_date": "Birth Date",
            "birth_place": "Birth Place",
            "social_security_number": "Social Security Number",
            "mobile_phone": "Mobile Phone",
            "show_mobile_phone": "Show Mobile Phone",
            "personal_email": "Personal Email",
            "active": "Active",
            "supervisor": "Supervisor",
            "work_phone": "Work Phone",
            "contract": "Contract",
            "week": "week",
            "recruitment": "Recruitment",
            "function": "Function",
            "diploma": "Diploma",
            "specification": "Specification",
            "transport": "Transport",
            "commuting": "Commuting",
            "marital_status": "Marital Status",
            "marital_status_since": "Marital Status Since",
            "partner_surname": "Partner Surname",
            "partner_name": "Partner Name",
            "partner_birth_date": "Partner Birth Date",
            "partner_profession": "Partner Profession",
            "partner_dependant": "Partner Dependant",
            "number_of_children_dependant": "Number of children dependant",
            "add_child": "Add Child",
            "edit_child": "Edit Child",
            "remove_child": "Remove Child",
            "extra_info": "Extra Info",
            "update_user": "Update User",
            "add_user": "Add User",
            "remove_user": "Are you sure to remove user?",
            "date": "Date",
            "add_public_holiday": "Add Public Holiday",
            "edit_public_holiday": "Edit Public Holiday",
            "public_holiday_name": "Public Holiday Name",
            "save_public_holiday": "Save Public Holiday",
            "update_public_holiday": "Update Public Holiday",
            "start_session": "Sign in to start your session",
            "password": "Password",
            "remember_me": "Remember Me",
            "sign_in": "Sign In",
            "forgot_password": "I forgot my password",
            "set_new_password": "Set New Password",
            "status_success": "New password has been successfully set.",
            "status_success2": "Please login using it.",
            "email_is_required": "Email is required.",
            "valid_email": "Must be a valid email.",
            "password_is_required": "Password is required.",
            "password_6_characters": "Password must be at least 6 characters long",
            "password_confirmation_required": "Password confirmation is required.",
            "pass_must_match": "New Password and its confirmation must match.",
            "reset_password": "Reset Password",
            "back_to_login": "Back to login",
            "recover_password": "Recover Password",
            "forgot_pass_text": "The email with reset password link has been just sent to the specified email address. Please check your email box.",
            "forgot_pass_text2": "Please enter your email address and we will sent you the link to reset your password",
            "copy_current": "Copy Current Year to Next Year",
            "edit_leavedays_for_user": "Edit Leavedays for User",
            "save_leavedays_for_user": "Save Leavedays for User",
            "default_leave_days": "Default Leave Days",
            "extra_leave_days_teamleader": "Extra Leave Days Teamleader",
            "file": "File",
            "requested_leaves": "Requested leaves",
            "grant": "Grant",
            "deny_request": "Deny Request",
            "reason": "Reason",
            "deny": "Deny",
            "are_you_sure": "Are you sure?",
            "are_you_sure_to_remove": "Are you sure to remove?",
            "save": "Save",
            "add": "Add",
            "save_changes": "Save Changes",
            "hours_small": "hours",
            "hour_small": "hour",
            "no_data_to_display": "No data to display",
            "show": "Show",
            "years": "Years",
            "year": "Year",
            "sign_out": "Sign out",
            "menu_translate": "Translate",
            "configurations": "Configurations",
            "active_users": "Active Users",
            "edit_user": "Edit User",
            "role": "Role",
            "leave_hours": "Leave Hours",
            "old_overtime": "Old Overtime",
            "default_leavedays": "Default Leave Days",
            "log_activities": "Log activities",
            "leave_request_small": "leave request",
            "authorization_token_expired": "The authorization token has been expired or it is invalid.",
            "please_login_again": "Please login again.",
            "error_403_title": "Oops! Not enough permissions.",
            "error_403_text": "Sorry, You do not have enough permissions to visit this page.",
            "profile_updated": "Profile Updated",
            "leave_request_upddated": "Leave Request Updated",
            "please_confirm": "Please Confirm",
            "remove_leave": "Are you sure to remove leave?",
            "leave_request_removed": "Leave Request Removed",
            "user_removed": "User Removed",
            "profile_of": "Profile Of",
            "updated": "Updated",
            "leavedays_for": "Leavedays for",
            "user_leaves_copy": "Are you sure to copy all active user leaves to the next year?",
            "sorry": "Sorry",
            "users_leaves_already_copied": "All active user leaves are already copied to the next year",
            "copied_year": "Copied Current Year to Next Year",
            "public_holiday_updated": "Public Holiday Updated",
            "public_holiday_added": "Public Holiday Added",
            "added": "Added",
            "want_to_logout": "Are you really want to log out?",
            "user_added": "User Added",
            "child_removed": "Child Removed",
            "child_added": "Child Added",
            "request_added": "Request Added",
            "leave_added": "Leave Added",
            "words_updated": "Words Updated",
            "language_already_exists": "This language already exists",
            "delete": "Delete",
            "delete_language": "Are you sure to delete this language?",
            "language_deleted": "Language Deleted",
            "customers": "Customers",
            "id": "Id",
            "customer_id": "Customer Id",
            "first_name": "First Name",
            "last_name": "Second Name",
            "email": "Email",
            "address": "Address",
            "phone": "Phone",
            "add_customer": "Add Customer",
            "edit_customer": "Edit Customer",
            "date_created": "Date Created",
            "created_at": "Date Created",
            "customer_added": "Customer Added",
            "customer_edited": "Customer Edited",
            "customer_deleted": "Customer Deleted",
            "update": "Update",
            "remove_customer": "Are you sure to delete customer?",
            "actions": "Actions",
            "rooms": "Rooms",
            "add_room": "Add room",
            "price_per_hour": "Price per hour",
            "price_30minutes_extra": "Price 30 minutes extra",
            "price_person_extra": "Price person extra",
            "min_hours": "Min hours",
            "max_hours": "Max hours",
            "min_persons": "Min_persons",
            "max_persons": "Max persons",
            "cleanup_time_minutes": "Cleanup time minutes",
            "base_price_amount_persons": "Base price amount persons",
            "edit_room": "Edit room",
            "room_edited": "Room edited",
            "remove_room": "Remove room",
        }
    },
    "nl": {
        "total": {
            "absences_overview": "Afwezigheidsoverzicht",
            "previous_day": "Vorige dag",
            "next_day": "Volgende dag",
            "no_data": "Geen gegevens",
            "back_on": "Terug op",
            "add_request": "Verzoek Toevoegen",
            "type": "Type",
            "start_date": "Begin datum",
            "end_date": "Einddatum",
            "number_of_days": "Aantal Dagen",
            "start_of_hour": "Begin Van Het Uur",
            "number_of_hours": "Aantal Uren",
            "send_for_approval": "Stuur Voor Goedkeuring",
            "update_leave_request": "Verzoek Verlofaanvraag Bijwerken",
            "save_and_confirm": "Opslaan en Bevestigen",
            "close": "Dichtbij",
            "user": "Gebruiker",
            "add_leave": "Verlof Toevoegen",
            "edit_request": "Wijzigingsverzoek",
            "welcome": "Welkom",
            "upcoming_confirmed_leaves": "Aankomende bevestigde bladeren",
            "no_confirmed_leaves": "Geen bevestigde bladeren",
            "days_ago": "dagen geleden",
            "start_in": "start in",
            "days": "Dagen",
            "leaves_awaiting_approval": "Bladeren in afwachting van goedkeuring",
            "no_leaves_awaiting_approval": "Geen bladeren in afwachting van goedkeuring",
            "leave_overview": "Verlofoverzicht",
            "leave_requests": "Verlofaanvragen",
            "start_hour": "Start uur",
            "hours": "Uren",
            "status": "Staat",
            "edit": "Bewerk",
            "remove": "Verwijderen",
            "overview": "Overzicht",
            "given": "Gegeven",
            "leave_days": "Verlof dagen",
            "extra_leave_days": "Extra Verlofdagen",
            "public_holidays": "Feestdagen",
            "taken_days": "Overgenomen Dagen",
            "total_days": "Totaal Aantal Dagen",
            "total_taken": "Totaal Genomen",
            "total_to_take": "Total To Take",
            "taken_hours": "Genomen Uren",
            "total_hours": "Uren in Totaal",
            "days_small": "dagen",
            "day_small": "dag",
            "on": "op",
            "leavetypes": "Leavetypes",
            "leavetype": "Leavetype",
            "user_selectable": "Gebruiker selecteerbaar",
            "in_hours": "In Uren",
            "add_leavetype": "Leavetype Toevoegen",
            "edit_leavetype": "Bewerk Leavetype",
            "leavetype_name": "Leavetype Naam",
            "save_leavetype": "Bewaar Leavetype",
            "update_leavetype": "Bijwerken Leavetype",
            "main_navigation": "Hoofdnavigatie",
            "user_menu": "Gebruikersmenu",
            "dashboard": "Dashboard",
            "absences_today": "Afwezigheden Vandaag",
            "supervisor_menu": "Supervisor Menu",
            "supervisor_overview": "Supervisor Overzicht",
            "admin_menu": "Beheerdermenu",
            "users": "Gebruikers",
            "public_holiday": "Wettelijke feestdag",
            "paycheck_process": "Paycheck-proces",
            "set_leavedays": "Verlofdagen instellen",
            "text_desc": "Verlengingsdagen toevoegen \/ bewerken voor gebruikers in",
            "show_processed": "Toon Verwerkt",
            "processed": "Verwerkte",
            "mark_done": "Markeer Klaar",
            "profile": "Profiel",
            "set_you_profile_settings": "stel je profielinstellingen in",
            "upload_new_avatar": "Upload nieuwe avatar",
            "surname": "Achternaam",
            "name": "Naam",
            "street": "Straat",
            "number": "Aantal",
            "box": "Doos",
            "postal_code": "Postcode",
            "city": "stad",
            "country": "land",
            "gender": "Geslacht",
            "nationality": "Nationaliteit",
            "birth_date": "Geboortedatum",
            "birth_place": "Geboorteplaats",
            "social_security_number": "Burgerservicenummer",
            "mobile_phone": "Mobiele telefoon",
            "show_mobile_phone": "Laat mobiele telefoon zien",
            "personal_email": "Persoonlijke email",
            "active": "Actief",
            "supervisor": "Leidinggevende",
            "work_phone": "Werktelefoon",
            "contract": "Contract",
            "week": "week",
            "recruitment": "Werving",
            "function": "Functie",
            "diploma": "Diploma",
            "specification": "Specificatie",
            "transport": "Vervoer",
            "commuting": "Woon-werkverkeer",
            "marital_status": "Burgerlijke staat",
            "marital_status_since": "Burgerlijke staat sinds",
            "partner_surname": "Achternaam van de partner",
            "partner_name": "Partnernaam",
            "partner_birth_date": "Partner geboortedatum",
            "partner_profession": "Partner Beroep",
            "partner_dependant": "Afhankelijk van de partner",
            "number_of_children_dependant": "Aantal kinderen afhankelijk",
            "add_child": "Voeg een kind toe",
            "edit_child": "Bewerk het kind",
            "remove_child": "Verwijder kind",
            "extra_info": "Extra info",
            "update_user": "Update Gebruiker",
            "add_user": "Voeg gebruiker toe",
            "remove_user": "Weet je zeker dat je de gebruiker moet verwijderen?",
            "date": "Datum",
            "add_public_holiday": "Voeg Feestdag Toe",
            "edit_public_holiday": "Wijzig Feestdag",
            "public_holiday_name": "Naam Feestdag",
            "save_public_holiday": "Feestdagen Redden",
            "update_public_holiday": "Bijwerken Feestdag",
            "start_session": "Meld u aan om uw sessie te starten",
            "password": "Wachtwoord",
            "remember_me": "Onthoud mij",
            "sign_in": "Aanmelden",
            "forgot_password": "Ik ben mijn wachtwoord vergeten",
            "set_new_password": "Stel een nieuw wachtwoord in",
            "status_success": "Nieuw wachtwoord is succesvol ingesteld.",
            "status_success2": "Log hier alsjeblieft in in.",
            "email_is_required": "E-mail is verplicht.",
            "valid_email": "Moet een geldige e-mail zijn.",
            "password_is_required": "Een wachtwoord is verplicht.",
            "password_6_characters": "Het wachtwoord moet op zijn minst 6 tekens lang zijn",
            "password_confirmation_required": "Wachtwoordbevestiging is vereist.",
            "pass_must_match": "Nieuw wachtwoord en de bevestiging moeten overeenkomen.",
            "reset_password": "Reset Wachtwoord",
            "back_to_login": "Terug naar Inloggen",
            "recover_password": "Herstel Wachtwoord",
            "forgot_pass_text": "De e-mail met reset-wachtwoordlink is zojuist naar het opgegeven e-mailadres verzonden. Controleer uw e-mailbox.",
            "forgot_pass_text2": "Voer je e-mailadres in en we sturen je de link om je wachtwoord opnieuw in te stellen",
            "copy_current": "Kopieer het huidige jaar naar volgend jaar",
            "edit_leavedays_for_user": "Verlengingsdagen bewerken voor gebruiker",
            "save_leavedays_for_user": "Verlofdagen voor gebruiker opslaan",
            "default_leave_days": "Standaard verlofdagen",
            "extra_leave_days_teamleader": "Extra Verlofdagen Teamleider",
            "file": "Het Dossier",
            "requested_leaves": "Gevraagde bladeren",
            "grant": "Verlenen",
            "deny_request": "Verzoek Weigeren",
            "reason": "Reden",
            "deny": "Ontkennen",
            "are_you_sure": "Weet je het zeker?",
            "are_you_sure_to_remove": "Weet je zeker dat je wilt verwijderen?",
            "save": "Opslaan",
            "add": "Toevoegen",
            "save_changes": "Wijzigingen opslaan",
            "hours_small": "uur",
            "hour_small": "uur",
            "no_data_to_display": "Geen gegevens om weer te geven",
            "show": "Laten zien",
            "years": "Jaren",
            "year": "Jaar",
            "sign_out": "Afmelden",
            "menu_translate": "Vertalen",
            "configurations": "Configuraties",
            "active_users": "Actieve Gebruikers",
            "edit_user": "Bewerk Gebruiker",
            "role": "Rol",
            "leave_hours": "Verlaat Uren",
            "old_overtime": "Oude Overuren",
            "default_leavedays": "Standaard Verlofdagen",
            "log_activities": "Log activiteiten",
            "leave_request_small": "verlof verzoek",
            "authorization_token_expired": "Het autorisatie-token is verlopen of het is ongeldig.",
            "please_login_again": "Log alsjeblieft nogmaals in.",
            "error_403_title": "Oops! Niet genoeg rechten.",
            "error_403_text": "Sorry, je hebt niet genoeg rechten om deze pagina te bezoeken.",
            "profile_updated": "Profiel geüpdatet",
            "leave_request_upddated": "Verlofaanvraag bijgewerkt",
            "please_confirm": "Bevestig Alstublieft",
            "remove_leave": "Weet je zeker dat je verlof wilt verwijderen?",
            "leave_request_removed": "Verzoek verlaten verwijderd",
            "user_removed": "Gebruiker verwijderd",
            "profile_of": "Profiel van",
            "updated": "Bijgewerkt",
            "leavedays_for": "Verlof dagen voor",
            "user_leaves_copy": "Weet u zeker dat u alle actieve gebruikersbladen naar het volgende jaar moet kopiëren?",
            "sorry": "Sorry",
            "users_leaves_already_copied": "Alle actieve gebruikersbladeren zijn al gekopieerd naar het volgende jaar",
            "copied_year": "Huidige jaar naar volgend jaar gekopieerd",
            "public_holiday_updated": "Feestdag bijgewerkt",
            "public_holiday_added": "Feestdagen toegevoegd",
            "added": "Toegevoegd",
            "want_to_logout": "Wil je echt uitloggen?",
            "user_added": "Gebruiker toegevoegd",
            "child_removed": "Kind verwijderd",
            "child_added": "Kind toegevoegd",
            "request_added": "Verzoek toegevoegd",
            "leave_added": "Laat Toegevoegd",
            "words_updated": "Woorden bijgewerkt",
            "language_already_exists": "Deze taal bestaat al",
            "delete": "Deze taal bestaat al",
            "delete_language": "Weet je zeker van deze taal?",
            "language_deleted": "Taal verwijderd"
        }
    }
}
