import VueRouter from 'vue-router';
import AuthService from './services/auth-service';



let publicRoutes = [
    {
        path: '/',
        redirect: to => {
            if (AuthService.isLoggedIn) {
                return '/customers';
            }
            else {
                return '/login';
            }
        }
    },
    {
        path: '/login',
        component: require('./public-components/pages/login')
    },
    {
        path: '/forgot-password',
        component: require('./public-components/pages/forgot-password')
    },
    {
        path: '/password/reset/:token',
        component: require('./public-components/pages/reset-password')
    },
];

let privateRoutes = [
    {
        path: '/customers',
        component: require('./private-components/pages/customers'),
        meta: { requiresAuth: true },
        name: 'Customers'
    },
    {
        path: '/rooms',
        component: require('./private-components/pages/rooms'),
        meta: { requiredAuth: true },
        name: 'Rooms'
    },
    {
        path: '/configurations/translate',
        component: require('./private-components/pages/configurations/translate'),
        meta: { requiresAuth: true },
        name: 'Translate'
    },

];

const router = new VueRouter({
    routes: publicRoutes.concat(privateRoutes),
    mode: 'history',
    linkActiveClass: 'active'
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth) && !AuthService.isLoggedIn) {
        next({ path: '/login'});
    }
    else if (to.path == '/login' && AuthService.isLoggedIn){
        next({ path: '/customers'});
    }
    else {
        next();
    }
});


export default router;
