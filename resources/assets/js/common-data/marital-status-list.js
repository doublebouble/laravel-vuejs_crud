const maritalStatusList = [
    {name: 'Ongehuwd', code: 'ongehuwd'},
    {name: 'Wettig gehuwd', code: 'wettig gehuwd'},
    {name: 'Partnerschap', code: 'partnerschap'},
    {name: 'Verweduwd na wettig huwelijk', code: 'verweduwd na wettig huwelijk'},
    {name: 'Verweduwd na partnerschap', code: 'verweduwd na partnerschap'},
    {name: 'Gescheiden na wettig huwelijk', code: 'gescheiden na wettig huwelijk'},
    {name: 'Gescheiden na partnerschap', code: 'gescheiden na partnerschap'},
];

export default maritalStatusList;