const AuthService = {
    isLoggedIn: false,
    login: function() { this.isLoggedIn = true },
    logout: function(isAutoLogout) {
        this.isLoggedIn = false;
        localStorage.clear();

        if (isAutoLogout === true) {
            localStorage.setItem('isAutoLogout', '1');
        }

        setTimeout(()=>{
            window.location.assign('/login');
        },100);
    },
    defineIsLoggedIn: function() {
        this.isLoggedIn = localStorage.hasOwnProperty('auth-token');
    }
};

export default AuthService;