@component('mail::message')

@if($emailData['supervisor_s']==1)
    # Dear {{$emailData['supervisor']->name}} {{$emailData['supervisor']->surname}}!

    # {{$emailData['user']->name}} {{$emailData['user']->surname}} added leave request

    You need to approve or reject the request.
@else
    # Success

    ## Leave request has been successfully created

    Dear {{$emailData['user']->name}}! This email confirms that you leave request has been successfully submitted.

@endif

    The request details:

    - Leave Type: {{$emailData['leaveRequest']->nameLeaveType}}
    - Start Date: {{$emailData['leaveRequest']->start_date}}
    - End Date: {{$emailData['leaveRequest']->end_date}}



@endcomponent