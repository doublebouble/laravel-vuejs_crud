@component('mail::message')

    @if($emailData['supervisor']!=1)
        # Leave request is {{$emailData['status']}}



        Dear {{$emailData['userOfRequest']->name}} {{$emailData['userOfRequest']->surname}}! This email confirms that your leave request has been {{$emailData['status']}}.
    @else
    # You {{$emailData['status']}} leave request for  {{$emailData['userOfRequest']->name}} {{$emailData['userOfRequest']->surname}}
    @endif


     The request details:

    - Type: {{$emailData['leaveRequest']['leave_type']['name']}}
    - Start Date: {{$emailData['leaveRequest']['start_date']}}
    - End Date: {{$emailData['leaveRequest']['end_date']}}
    - #Days: {{$emailData['leaveRequest']['days']}}
    - Start Hour: {{$emailData['leaveRequest']['start_hour']}}
    - #Hours: {{$emailData['leaveRequest']['hours']}}

    @if($emailData['leaveRequest']['reason'])
    Reason - {{$emailData['leaveRequest']['reason']}}
    @endif

    @if($emailData['supervisor']!=1)
    You can update your request and resend it
    @endif

@endcomponent