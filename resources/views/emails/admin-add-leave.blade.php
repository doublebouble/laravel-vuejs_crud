@component('mail::message')


@if($emailData['admin_s']!=1 && $emailData['supervisor_s']!=1)
    # Leave request is granted

    Dear {{$emailData['user']->name}} {{$emailData['user']->surname}}! This email confirms that your leave request is granted.

@elseif($emailData['admin_s']!=1 && $emailData['supervisor_s']==1)
    # Dear {{$emailData['supervisor']->name}} {{$emailData['supervisor']->surname}}!

    # {{$emailData['admin']->name}} {{$emailData['admin']->surname}} added and granted leave request for  {{$emailData['user']->name}} {{$emailData['user']->surname}}
@else
    # Dear {{$emailData['admin']->name}} {{$emailData['admin']->surname}}!

    # You added and granted leave request for  {{$emailData['user']->name}} {{$emailData['user']->surname}}

@endif


    The request details:

    - Type: {{$emailData['leaveRequest']['nameLeaveType']}}
    - Start Date: {{$emailData['leaveRequest']['start_date']}}
    - End Date: {{$emailData['leaveRequest']['end_date']}}
    - #Days: {{$emailData['leaveRequest']['days']}}
    - Start Hour: {{$emailData['leaveRequest']['start_hour']}}
    - #Hours: {{$emailData['leaveRequest']['hours']}}



@endcomponent