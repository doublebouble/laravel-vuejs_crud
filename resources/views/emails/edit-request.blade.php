@component('mail::message')

@if($emailData['supervisor_s']==1)
    # Dear {{$emailData['supervisor']->name}} {{$emailData['supervisor']->surname}}!

    # {{$emailData['user']->name}} {{$emailData['user']->surname}} edited leave request

    You need to approve or reject the request.
    Previous reason of reject is {{$emailData['leaveRequest']->deny_reason}}
@else
    # Dear {{$emailData['user']->name}}!

    # Leave request has been successfully edited


@endif

    The request details:

    - Leave Type: {{$emailData['leaveRequest']->nameLeaveType}}
    - Start Date: {{$emailData['leaveRequest']->start_date}}
    - End Date: {{$emailData['leaveRequest']->end_date}}



@endcomponent