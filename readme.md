php #Sporta Leave

### Laravel 5.6 + VueJs 2

### Installation

```
composer install
npm install
bower install
```

Create `.env` file (can be based on `.env.example`)
```
php artisan key:generate
```

Create .env file using .env.example as the base.

Set DB credentials within .env file

Run migrations

```
php artisan migrate
```

Create storage link

```
php artisan storage:link
```


### Dev Build

```
npm run dev
```

or run watcher

````
npm run watch
````

### Production Build

go to project url in browser or run the command:

```
npm run prod
```

### Structure

VueJs files are stored in  **/resources/assets/js** folder

### Translation
The translations for different languages are stored at /resources/assests/lang folder. There are folders, that are named with appropriate language.

If any modifications are made with translation files, please the following command in order to apply changes at Vue:

```
php artisan vue-i18n:generate
```

To dynamically change the translation of words / add / delete languages, please be sure to set the following permissions:

```
php artisan vue-i18n:generate

sudo chmod 777 resources/lang/ -R
sudo chmod 777 resources/assets/js/vue-i18n-locales.generated.js 

sudo chmod 777 public/mix-manifest.json
sudo chmod 777 public/js/app.js
sudo chmod 777 public/css/app.css
sudo chmod 777 public/js/vendor.js
sudo chmod 777 public/fonts/ -R
```

###E-Mailing

Currently emailing is used for:
- reset password
- notify user for new leave request has been created
- notify user and supervisor, when supervisor granted or denied a request leave
- notify user, supervisor and admin, when admin created a new leave

for dev purpose please set-up appropriate smtp.mailtrap.io settings in .env file

###.env file

the following additional fields are obligate to be filled out within .env file:
- APP_URL
- MAIL_DRIVER
- MAIL_HOST
- MAIL_PORT
- MAIL_USERNAME
- MAIL_PASSWORD
- MAIL_ENCRYPTION